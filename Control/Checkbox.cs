﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Block.Base;
using System.Windows.Forms;
using Block.Helper;


namespace Block.Control
{
    class Checkbox : Control
    {
        public bool CheckStatus { get; set; }
        public string Text { get; set; }
        public FTFont Font { get; set; }

        Texture2D Checked;
        Texture2D Unchecked;
        Text txt;
        public override void LoadContent()
        {
            Checked = Loader.LoadTexture(this.GraphicsDevice, "Content\\checkbox-checked.png");
            Unchecked = Loader.LoadTexture(this.GraphicsDevice, "Content\\checkbox-unchecked.png");
            txt = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = this.Color, Position = new Vector2(this.Rectangle.X+this.Rectangle.Width+5, this.Rectangle.Y), Font = Font,text=Text };

            base.LoadContent();
        }
        public override void Draw(GameTime gameTime)
        {
            if (Texture == null)
                Texture = Unchecked;

            if (CheckStatus == true)
                Texture = Checked;
            else
                Texture = Unchecked;

            base.Draw(gameTime);
            txt.Draw(gameTime);
        }

        public override void OnClick(object sender, EventArgs e)
        {
            if( isClicked==false)
            {
                CheckStatus = !CheckStatus;
                base.OnClick(sender, e);
            }

        }
        
        }

    }

