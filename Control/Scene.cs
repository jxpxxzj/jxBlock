﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Block.Base;


namespace Block.Control
{
    abstract class Scene : Control
    {
        public event EventHandler Load;
        public event EventHandler Unload;

        public virtual void OnLoad(object sender, EventArgs e)
        {
            if(Load!=null)
                Load(sender, e);
        }

        public virtual void OnUnload(object sender, EventArgs e)
        {
            if(Unload!=null)
                Unload(sender, e);
        }

    }  
}
