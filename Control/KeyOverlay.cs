﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Block.Base;

namespace Block.Control
{
    class KeyOverlay : Overlay
    {
        public Keys KeyBinding{get;set;}

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(KeyBinding))
            {
                this.Color = Config.KeyActive;
                OnKeyDown(this, EventArgs.Empty);
                if (isKeyDown==false)
                    if(Enabled==true)
                        HitCount++;
                isKeyDown = true;
            }
            else
            {
                this.Color = Color.White;
                OnKeyUp(this, EventArgs.Empty);
                isKeyDown = false;
            }
        }

    }
}
