﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Block.Control
{
    class DisappearImage : Image
    {
        public TimeSpan ShowTime { get; set; }
        Stopwatch timer=new Stopwatch();
        public bool Disappeared { get; set; }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            timer.Start();
            if (timer.Elapsed < ShowTime)
                base.Draw(gameTime);
            else
                Disappeared = true;

        }
    }
}
