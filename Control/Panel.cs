﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Block.Control;
using Block.Base;

namespace Block.Control
{
    class Panel : Control
    {
        public List<Control> Controls { get; set; }

        public override void LoadContent()
        {
            Controls = new List<Control>();
        }
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            base.Draw(gameTime);
            foreach (Control s in Controls)
            {
                s.Draw(gameTime);
            }
            
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            foreach (Control s in Controls)
            {
                s.Update(gameTime);
            }
            base.Update(gameTime);
        }
    }
}
