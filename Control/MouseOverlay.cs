﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Block.Base;

namespace Block.Control
{
    class MouseOverlay : Overlay
    {
        public enum MouseButton
        {
            LeftButton = 0,
            RightButton = 1
        }
        public MouseButton MouseBinding { get; set; }
       
        public override void Update(GameTime gameTime)
        {
            if (((Mouse.GetState().LeftButton==ButtonState.Pressed) && (MouseBinding==MouseButton.LeftButton))
                || ((Mouse.GetState().RightButton==ButtonState.Pressed) && (MouseBinding==MouseButton.RightButton)))
            {
                this.Color = Config.MouseActive;
                OnMouseDown(this, new System.Windows.Forms.MouseEventArgs(System.Windows.Forms.MouseButtons.None, 0, 0, 0, 0));
                if (isMouseDown == false)
                    if (Enabled == true)
                    HitCount++;
                isMouseDown = true;
            }
            else
            {
                this.Color = Color.White;
                OnMouseUp(this, EventArgs.Empty);
                isMouseDown = false;
            }
        }
    }
}
