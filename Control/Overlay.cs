﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Block.Base;
using Block.Helper;

namespace Block.Control
{
    class Overlay:Shape
    {
        public FTFont Font { get; set; }
        protected Text ShowText;
        public string Text { get; set; }
        public bool Enabled =true;
        protected int HitCount = 0;
        
        public override void LoadContent()
        {
            ShowText = new Text { text = this.Text, SpriteBatch = this.SpriteBatch, GraphicsDevice = this.GraphicsDevice, Color = Color.Black, Font = this.Font };
            ShowText.Position = new Vector2(this.Rectangle.X + 5, this.Rectangle.Y + 6);
            base.LoadContent();
        }
        public override void Draw(GameTime gameTime)
        {
            this.DrawRectangle(this.Rectangle, this.Color);
            this.FillRectangle(this.Rectangle, this.Color);
            if (HitCount != 0)
                ShowText.text = HitCount.ToString();
            else
                ShowText.text = Text;
            ShowText.Draw(gameTime);
        }
    }
}
