﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
#endregion

namespace Block
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                var game = new Game_DX();
                game.Run();
            }
            catch (Exception ex)
            {
                string para = @ex.Message +
                    ex.StackTrace;
                //System.Windows.Forms.MessageBox.Show(para);
                Application.Exit();
#if DEBUG
                throw ex;
#endif
            }

        }
    }
#endif
}
