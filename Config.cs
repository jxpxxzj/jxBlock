﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using System.Windows.Forms;

       
namespace Block
{
    class ConfigLoader
    {
        #region IniController
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        private void IniWriteValue(string Section, string Key, string Value, string filepath)
        {
            WritePrivateProfileString(Section, Key, Value, filepath);
        }
        private string IniReadValue(string Section, string Key, string filepath)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, filepath);
            return temp.ToString();
        }
#endregion 

        public string BGPFile { get; set; }
        public string BGMFile { get; set; }
        public string SampleFile { get; set; }
        public string MissImage { get; set; }
        public string MissSample { get; set; }

        public string NoFailImage { get; set; }
        public string HardRockImage { get; set; }
        public string HiddenImage { get; set; }
        public string FlashLightImage { get; set; }
        public string AutoImage { get; set; }
        public string RelaxImage { get; set; }

        public Color KeyActive { get; set; }
        public Color MouseActive { get; set; }
        public Color BlackTile { get; set; }
        public Color BlackTileClicked { get; set; }
        public Color WhiteTile { get; set; }
        public Color WhiteTileClicked { get; set; }
        public Color FontColor { get; set; }

        public int GameMode { get; set; }
        public int TileCount { get; set; }

        public int MoveVelocity { get; set; }

        public int LineLength { get; set; }
        public int ElapsedMilliseconds { get; set; }

        
        public bool NoFail { get; set; }
        public bool HardRock { get; set; }
        public bool Hidden { get; set; }
        public bool FlashLight { get; set; }
        public bool Auto { get; set; }
        public bool Relax { get; set; }

        public int BufferSize { get; set; }


        public ConfigLoader(string FileName)
        {

            this.BGPFile = IniReadValue("Interface", "BGP", FileName);
            this.BGMFile = IniReadValue("Interface", "BGM", FileName);
            this.SampleFile = IniReadValue("Interface", "Sample", FileName);
            this.MissImage = IniReadValue("Interface", "MissImage", FileName);
            this.MissSample = IniReadValue("Interface", "MissSample", FileName);

            this.AutoImage = IniReadValue("Interface", "AutoLogo", FileName);
            this.RelaxImage = IniReadValue("Interface", "RelaxLogo", FileName);
            this.NoFailImage = IniReadValue("Interface", "NoFailLogo", FileName);
            this.HardRockImage = IniReadValue("Interface", "HardRockLogo", FileName);
            this.HiddenImage = IniReadValue("Interface", "HiddenLogo", FileName);
            this.FlashLightImage = IniReadValue("Interface", "FlashLightLogo", FileName);

            this.KeyActive = ReadIniColor("KeyActive", FileName);
            this.MouseActive = ReadIniColor("MouseActive", FileName);
            this.BlackTile = ReadIniColor("BlackTile", FileName);
            this.BlackTileClicked = ReadIniColor("BlackTileClicked", FileName);
            this.WhiteTile = ReadIniColor("WhiteTile", FileName);
            this.WhiteTileClicked = ReadIniColor("WhiteTileClicked", FileName);
            this.FontColor = ReadIniColor("FontColor", FileName);

            this.GameMode = Convert.ToInt32(IniReadValue("GamePlay.Universal", "GameMode", FileName));
            this.TileCount = Convert.ToInt32(IniReadValue("GamePlay.Universal", "TileCount", FileName));

            this.MoveVelocity = Convert.ToInt32(IniReadValue("GamePlay.Arcade", "MoveVelocity", FileName));

            this.LineLength = Convert.ToInt32(IniReadValue("GamePlay.Classic", "LineLength", FileName));

            this.ElapsedMilliseconds = Convert.ToInt32(IniReadValue("GamePlay.Zen", "ElapsedMilliseconds", FileName));

            this.NoFail = Convert.ToBoolean(IniReadValue("Mods", "NoFail", FileName));
            this.Auto = Convert.ToBoolean(IniReadValue("Mods", "Auto", FileName));
            this.Relax = Convert.ToBoolean(IniReadValue("Mods", "Relax", FileName));
            this.Hidden = Convert.ToBoolean(IniReadValue("Mods", "Hidden", FileName));
            this.HardRock = Convert.ToBoolean(IniReadValue("Mods", "HardRock", FileName));
            this.FlashLight = Convert.ToBoolean(IniReadValue("Mods", "FlashLight", FileName));

            this.BufferSize = Convert.ToInt32(IniReadValue("Settings", "BufferSize", FileName));
        }
        #region Convertor
        private Color ReadIniColor(string Key,string FileName)
        {
            return ConvertToColor(IniReadValue("Color", Key, FileName));
        }

        private Color ConvertToColor(string ColorString)
        {
            string[] b = ColorString.Split(',');
            Color temp = new Color { R = Convert.ToByte(b[0]), G = Convert.ToByte(b[1]), B = Convert.ToByte(b[2]), A = Convert.ToByte(b[3]) };
            return temp;
        }
        #endregion 
    }
    static class Config
    {
        public static string BGPFile = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).BGPFile;
        public static string BGMFile= new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).BGMFile;
        public static string SampleFile= new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).SampleFile;
        public static string MissImage = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).MissImage;
        public static string MissSample = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).MissSample;

        public static string AutoLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).AutoImage;
        public static string RelaxLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).RelaxImage;
        public static string NoFailLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).NoFailImage;
        public static string HardRockLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).HardRockImage;
        public static string HiddenLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).HiddenImage;
        public static string FlashLightLogo = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).FlashLightImage;

        public static Color KeyActive = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).KeyActive;
        public static Color MouseActive = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).MouseActive;
        public static Color BlackTile = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).BlackTile;
        public static Color BlackTileClicked = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).BlackTileClicked;
        public static Color WhiteTile = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).WhiteTile;
        public static Color WhiteTileClicked = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).WhiteTileClicked;
        public static Color FontColor = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).FontColor;

        public static int GameMode = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).GameMode;
        public static int TileCount = new ConfigLoader(string.Concat(Application.StartupPath,"\\config.ini")).TileCount;

        public static int MoveVelocity = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).MoveVelocity;
        public static int LineLength = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).LineLength;
        public static int ElapsedMilliseconds = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).ElapsedMilliseconds;

        public static bool NoFail = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).NoFail;
        public static bool HardRock = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).HardRock;
        public static bool Hidden = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).Hidden;
        public static bool FlashLight = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).FlashLight;
        public static bool Auto = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).Auto;
        public static bool Relax = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).Relax;

        public static int BufferSize = new ConfigLoader(string.Concat(Application.StartupPath, "\\config.ini")).BufferSize;

    }
}
