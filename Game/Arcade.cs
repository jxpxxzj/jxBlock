﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Diagnostics;
using Block.Control;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace Block.Game
{
    class Arcade:PipeBase 
    {
        public override string ToString()
        {
            return "Arcade";
        }
        public override void Draw(GameTime gameTime)
        {
    
            base.Draw(gameTime);
        }
        public override void LoadContent()
        {
            base.LoadContent();
            for (int i = 0; i < Config.BufferSize; i++)
            {
                BlockLine block = new BlockLine { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(this.Rectangle.X, 440 - 80 * i, 60, 80), BlockCount = this.BlockCount, MoveVelocity = this.MoveVelocity };
                block.LoadContent();
                block.BlackClicked += block_BlackClicked;
                block.WhiteClicked += block_WhiteClicked;
                block.Missed += block_Missed;
                BlockLine.Add(block);
            }
            BlockLine[0].BlackClicked += First_BlackClicked;
            BlockLine[0].WhiteClicked += First_WhiteClicked;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if ((LineLength == 0) && (BlockLine.Count < Config.BufferSize))
            {
                while (BlockLine.Count < Config.BufferSize)
                {
                    BlockLine block = new BlockLine { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(this.Rectangle.X, BlockLine[BlockLine.Count - 1].Rectangle.Y - 80, 60, 80), BlockCount = this.BlockCount, MoveVelocity = this.MoveVelocity };
                    block.LoadContent();
                    block.BlackClicked += block_BlackClicked;
                    block.WhiteClicked += block_WhiteClicked;
                    block.Missed += block_Missed;
                    BlockLine.Add(block);
                }
                NextIndex--;
            }
            if(Stopped==false)
            {
                if ((BlockLine[NextIndex].Rectangle.Y < 600) && (BlockLine[NextIndex].Rectangle.Y > -80) && Already && hasFailed == false)
                    BlockLine[NextIndex].Update(gameTime);
                if (Mods.Auto == true && BlockLine[NextIndex].Rectangle.Y > 420)
                {
                    BlockLine[NextIndex].SendResult(Game.BlockLine.HitResult.Black);
                }
                if (Mods.Relax == true && BlockLine[NextIndex].BlockList[BlockLine[NextIndex].BlackIndex].Rectangle.Contains(Mouse.GetState().X, Mouse.GetState().Y))
                {
                    BlockLine[NextIndex].SendResult(Game.BlockLine.HitResult.Black);
                }
                for (int i = 0; i < BlockLine.Count; i++)
                {
                        BlockLine[i].MoveBlock();
                        if (BlockLine[i].Rectangle.Y > 600)
                            BlockLine.RemoveAt(i);
                }
            }
            else
                if(ElapsedMilliseconds==0)
                    BlockLine[0].Update(gameTime);
                   
                
        }
    }
}
