﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Block.Control;
using Block.Audio;
using Microsoft.Xna.Framework;

namespace Block.Game
{
    class BlockLine:Control.Control 
    {
        public enum HitResult
        {
            Black=0,White=1,Missed=2
        }
        public int BlockCount { get; set; }
        public List<ObjectBlock> BlockList { get; set; }
        public int BlackIndex{get; protected set;}
        public int MoveVelocity { get; set; }

        public event EventHandler WhiteClicked;
        public event EventHandler BlackClicked;
        public event EventHandler Missed;

        Image MissImage;
        HitResult res;
        AudioPlayer SamplePlayer;

        public bool hasClicked = false;
        public override void LoadContent()
        {
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));

            MissImage = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Texture = Loader.LoadTexture(this.GraphicsDevice, Config.MissImage), Color = Color.White };
            
            BlackIndex = ran.Next(BlockCount);
            BlockList=new List<ObjectBlock>();
            for(int i=0;i<BlockCount;i++)
            {
                if(i==BlackIndex)
                {
                    BlockList.Add(new ObjectBlock { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.BlackTile ,HitColor=Config.BlackTileClicked, Rectangle = new Rectangle(this.Rectangle.X + i * 60, this.Rectangle.Y, 60, 80) }); 
                    BlockList[i].Click += OnBlackClick;
                }
                else
                {
                    BlockList.Add(new ObjectBlock { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.WhiteTile ,HitColor=Config.WhiteTileClicked, Rectangle = new Rectangle(this.Rectangle.X + i * 60, this.Rectangle.Y, 60, 80) });
                    BlockList[i].Click += OnWhiteClick;
                }
                BlockList[i].LoadContent();
            }
            base.LoadContent();
        }
        public override void Draw(GameTime gameTime)
        {
            foreach (ObjectBlock s in BlockList)
                s.Draw(gameTime);
            if ((hasClicked == true) && (res==HitResult.White))
                MissImage.Draw(gameTime);
        }

        public void MoveBlock()
        {
            this.Rectangle = new Rectangle(this.Rectangle.X, this.Rectangle.Y + MoveVelocity, this.Rectangle.Width, this.Rectangle.Height);
            RefreshPosition();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (hasClicked == false)
            {
                foreach (ObjectBlock s in BlockList)
                    s.Update(gameTime);
            }
            if ((this.Rectangle.Y >600-this.MoveVelocity-1) && hasClicked == false)
                OnMissed(this, EventArgs.Empty);

        }
        protected void OnBlackClick(object sender,EventArgs e)
        {
            if (BlackClicked != null) 
            {
                BlackClicked(sender, e);  
            }
            hasClicked = true;
            SamplePlayer = new AudioPlayer { Stream = Loader.LoadAudioStream(Config.SampleFile) };
            SamplePlayer.Play(false);
        }
        protected void OnWhiteClick(object sender, EventArgs e)
        {
            if (WhiteClicked != null) 
            {
                WhiteClicked(sender,e);
            }
            hasClicked = true;
            MissImage.Rectangle = new Rectangle(this.Rectangle.X+60*BlackIndex+1, this.Rectangle.Y+10, MissImage.Texture.Width, MissImage.Texture.Height);
            res = HitResult.White;
            SamplePlayer = new AudioPlayer { Stream = Loader.LoadAudioStream(Config.MissSample) };
            SamplePlayer.Play(false);
        }
        protected void OnMissed(object sender,EventArgs e)
        {
            if (Missed != null)
                Missed(sender, e);
            hasClicked = true;
        }
        public void RefreshPosition()
        {
            foreach(ObjectBlock s in BlockList)
            {
                s.Rectangle = new Rectangle(s.Rectangle.X, this.Rectangle.Y, 60, 80);
            }
            MissImage.Rectangle = new Rectangle(MissImage.Rectangle.X, this.Rectangle.Y+10, MissImage.Texture.Width, MissImage.Texture.Height);
        }
        public void SendResult(HitResult res)
        {
            if (hasClicked==false)
            {
                this.res = res;
                hasClicked = true;
                BlockList[BlackIndex].OnClick(this, EventArgs.Empty);
                //if (res == HitResult.Black)
                //    OnBlackClick(this, EventArgs.Empty);
                //if (res == HitResult.White)
                //    OnWhiteClick(this, EventArgs.Empty);
                //if (res == HitResult.Missed)
                //    OnMissed(this, EventArgs.Empty);
            }
        }
    }
}
