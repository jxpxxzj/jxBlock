﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Numerics;
using Block.Control;


namespace Block.Game
{
    class PipeBase : Control.Control
    {
        public List<BlockLine> BlockLine { get; set; }
        public int LineLength { get; set; }
        public int MoveVelocity { get; set; }
        public int BlockCount { get; set; }
        public GameStatus Status { get; protected set; }
        public enum GameStatus
        {
            Start=0,Stop=1,Failed=2
        }

        public Mod Mods { get; set; }

        public event EventHandler Failed;
        protected void OnFailed(object sender, EventArgs e)
        {
            if (Failed != null)
                Failed(sender, e);
            foreach (BlockLine s in BlockLine)
                s.MoveBlock();
            hasFailed = true;
            Stop();
            Status = GameStatus.Failed;
        }

        public int MissCount { get; protected set; }
        public int BlackCount { get; protected set; }
        public int WhiteCount { get; protected set; }

        public int MaxCombo { get; protected set; }
        public int TotalCombo { get; protected set; }
        public BigInteger Score { get; protected set; }
        public double Accuracy
        {
            get
            {
                if (BlackCount + WhiteCount+MissCount != 0)
                    return Convert.ToDouble((1.0 * BlackCount / (BlackCount + WhiteCount + MissCount)) * 100);
                else
                    return 100;
            }
        }
        public double Speed
        {
            get
            {
                double res = (BlackCount) / ((1.0 * timer.ElapsedMilliseconds) / 1000);
                if (timer.ElapsedMilliseconds != 0)
                    return res;
                else
                    return 0;
            }
        }
        public double ElapsedMilliseconds
        {
            get
            {
                return timer.ElapsedMilliseconds;
            }
        }

        protected bool Already = true;
        public int NextIndex{get;protected set;}
        protected Stopwatch timer = new Stopwatch();
        public bool Stopped = true;
        public bool hasFailed = false;
        
        public void Stop()
        {
            timer.Stop();
            Stopped = true;
            Status = GameStatus.Stop;
        }
        public void Start()
        {
            Stopped = false;
            Already = true;
            hasFailed = false;
            Status = GameStatus.Start;
            //timer.Start();
        }

        public override void LoadContent()
        {
            Status = GameStatus.Stop;
            Mods.LoadContent();
            BlockLine = new List<Game.BlockLine>();
            
        }
        public override void Draw(GameTime gameTime)
        {
            for (int i = 0; i < BlockLine.Count; i++)
            {
                if ((BlockLine[i].Rectangle.Y < 600) && (BlockLine[i].Rectangle.Y > -80))
                    BlockLine[i].Draw(gameTime);
            }
        }

        public virtual void MoveBlock()
        {

        }
        public virtual void block_Missed(object sender, EventArgs e)
        {
            timer.Start();
            MissCount++;
            NextIndex++;
            TotalCombo = 0;
            Already = false;
            OnClick(sender, e);
            if (Mods.NoFail == false)
                OnFailed(this, EventArgs.Empty);
            
        }

        public virtual void block_WhiteClicked(object sender, EventArgs e)
        {
            timer.Start();
            WhiteCount++;
            NextIndex++;
            TotalCombo = 0;
            Already = false;
            OnClick(sender, e);
            if (Mods.NoFail == false)
                OnFailed(this, EventArgs.Empty);
            
        }

        public virtual void block_BlackClicked(object sender, EventArgs e)
        {
            timer.Start();
            BlackCount++;
            TotalCombo++;
            NextIndex++;
            Score += TotalCombo;
            if (TotalCombo > MaxCombo)
                MaxCombo = TotalCombo;
            Already = false;
            OnClick(sender, e);
            
        }

        public override void Update(GameTime gameTime)
        {
            if (Mouse.GetState().LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Released
                && Mouse.GetState().RightButton == Microsoft.Xna.Framework.Input.ButtonState.Released
                && Keyboard.GetState().IsKeyUp(Keys.X)
                && Keyboard.GetState().IsKeyUp(Keys.Z))
            {
                Already = true;
            }
        }

        protected void First_BlackClicked(object sender, EventArgs e)
        {
            Start();
            block_BlackClicked(sender, e);
            NextIndex--;
        }

        protected void First_WhiteClicked(object sender, EventArgs e)
        {
            Start();
            block_WhiteClicked(sender, e);
            NextIndex--;
        }
    }
}
