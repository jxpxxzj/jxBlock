﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Block.Control;
using Block.Base;

namespace Block.Game
{
    class Mod :Control.Control
    {
        public bool NoFail { get; set; }
        public bool HardRock { get; set; }
        public bool Hidden {get;set;}
        public bool FlashLight { get; set; }
        public bool Auto { get; set; }
        public bool Relax { get; set; }


        Image AutoLogo;
        Image RelaxLogo;
        Image NoFailLogo;
        Image HardRockLogo;
        Image HiddenLogo;
        Image FlashLightLogo;

        int ModCount = 0;

        public override void LoadContent()
        {
            AutoLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            AutoLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.AutoLogo);

            RelaxLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            RelaxLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.RelaxLogo);

            NoFailLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            NoFailLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.NoFailLogo);

            HardRockLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            HardRockLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.HardRockLogo);

            HiddenLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            HiddenLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.HiddenLogo);

            FlashLightLogo = new Image { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Color.White };
            FlashLightLogo.Texture = Loader.LoadTexture(this.GraphicsDevice, Config.FlashLightLogo);

            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            ModCount = 0;
            if (Relax==true)
            {
                ModCount++;
                RelaxLogo.Rectangle = new Rectangle(this.Rectangle.X+ModCount*20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                RelaxLogo.Draw(gameTime);
            }
            if (NoFail == true)
            {
                ModCount++;
                NoFailLogo.Rectangle = new Rectangle(this.Rectangle.X + ModCount * 20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                NoFailLogo.Draw(gameTime);
            }
            if (HardRock == true)
            {
                ModCount++;
                HardRockLogo.Rectangle = new Rectangle(this.Rectangle.X + ModCount * 20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                HardRockLogo.Draw(gameTime);
            }
            if (Hidden == true)
            {
                ModCount++;
                HiddenLogo.Rectangle = new Rectangle(this.Rectangle.X + ModCount * 20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                HiddenLogo.Draw(gameTime);
            }
            if (FlashLight == true)
            {
                ModCount++;
                FlashLightLogo.Rectangle = new Rectangle(this.Rectangle.X + ModCount * 20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                FlashLightLogo.Draw(gameTime);
            }
            if (Auto == true)
            {
                ModCount++;
                AutoLogo.Rectangle = new Rectangle(this.Rectangle.X + ModCount * 20, this.Rectangle.Y, AutoLogo.Texture.Width, AutoLogo.Texture.Height);
                AutoLogo.Draw(gameTime);
            }
        }
        
    }
}
