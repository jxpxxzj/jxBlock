﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Block.Game
{
    class Classic : PipeBase
    {
        public override string ToString()
        {
            return "Classic";
        }
        public override void LoadContent()
        {
            base.LoadContent();
            LineLength = Config.LineLength;
            MoveVelocity = 20;
            for (int i = 0; i < LineLength; i++)
                {
                    BlockLine block = new BlockLine { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(this.Rectangle.X, 440 - 80 * i, 60, 80), BlockCount = this.BlockCount, MoveVelocity = this.MoveVelocity };
                    block.LoadContent();
                    block.BlackClicked += block_BlackClicked;
                    block.WhiteClicked += block_WhiteClicked;
                    block.Missed += block_Missed;
                    BlockLine.Add(block);
                }
            BlockLine[0].BlackClicked += First_BlackClicked;
            BlockLine[0].WhiteClicked += First_WhiteClicked;
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (NextIndex ==LineLength)
            {
                Stop();
                MoveBlock();
            }
            if (Stopped == false)
            {
                if (BlockLine[NextIndex].Rectangle.Y < 440)
                {
                    for (int i = 0; i < BlockLine.Count; i++)
                    {
                        BlockLine[i].MoveBlock();   
                    }
                }
                if ((BlockLine[NextIndex].Rectangle.Y < 600) && (BlockLine[NextIndex].Rectangle.Y > -80) && Already && hasFailed == false)
                    BlockLine[NextIndex].Update(gameTime);
                if (Mods.Auto == true && BlockLine[NextIndex].Rectangle.Y > 420)
                {
                    BlockLine[NextIndex].SendResult(Game.BlockLine.HitResult.Black);
                }
                if (Mods.Relax == true && BlockLine[NextIndex].BlockList[BlockLine[NextIndex].BlackIndex].Rectangle.Contains(Mouse.GetState().X, Mouse.GetState().Y))
                {
                    BlockLine[NextIndex].SendResult(Game.BlockLine.HitResult.Black);
                }       
            }
            else
                if (ElapsedMilliseconds == 0)
                    BlockLine[0].Update(gameTime);
         }
        public override void MoveBlock()
        {
            foreach (BlockLine s in BlockLine)
            {
                s.Rectangle = new Rectangle(s.Rectangle.X, s.Rectangle.Y + 80, s.Rectangle.Width, s.Rectangle.Height);
                s.RefreshPosition();
            }
        }
      }
    }

