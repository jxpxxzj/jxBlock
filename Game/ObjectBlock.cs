﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Block.Control;
using Block.Audio;

namespace Block.Game
{
    class ObjectBlock:Shape
    {
        public Color HitColor { get; set; }
        public override void LoadContent()
        {
            base.LoadContent();
        }
        public override void OnClick(object sender, EventArgs e)
        {
            if (isClicked==false)
            {
                base.OnClick(sender, e);
                this.Color = HitColor;

            }
        }

        public override void Update(GameTime gameTime)
        {
            if (Rectangle.Contains(Mouse.GetState().X, Mouse.GetState().Y))
            {
                if (Mouse.GetState().LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed
                    || Mouse.GetState().RightButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed
                    || Keyboard.GetState().IsKeyDown(Keys.X)
                    || Keyboard.GetState().IsKeyDown(Keys.Z))
                {
                    OnClick(this, EventArgs.Empty);
                    base.isClicked = true;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            this.DrawRectangle(this.Rectangle, Color.Gray);
            this.FillRectangle(this.Rectangle, this.Color);
        }

    }
}
