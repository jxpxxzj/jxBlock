﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Block.Base;
using Microsoft.Xna.Framework;


namespace Block.Helper
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    class FpsCounter : Text
    {

        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;

        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            frameCounter++;

            text = string.Concat(Convert.ToString(frameRate), "fps") ;
            if (gameTime.IsRunningSlowly == true)
                this.Color = Color.Red;
            else
                this.Color = Config.FontColor;
            base.Draw(gameTime);
        }
    }
}
