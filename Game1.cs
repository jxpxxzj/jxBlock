﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Block.Base;
using Block.Control;
using Un4seen.Bass;
using Block.Helper;
#endregion

namespace Block
{
   
    public class Game_DX : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        FTFont Font;
        OriginScene sc;
        Scene c;


        public Game_DX()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            
            this.graphics.PreferredBackBufferHeight = 600;
            this.graphics.PreferredBackBufferWidth = 800;
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            this.IsMouseVisible = true;
            this.IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;
            TargetElapsedTime = TimeSpan.FromMilliseconds(1);
            graphics.ApplyChanges();
            this.Window.Title = "Don't Tap The White Tile v0.2";
            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            BassNet.Registration("28256042@qq.com", "2X123012150022");
            Font = new FTFont(this.GraphicsDevice, "msyh.ttc", 12,-20);

            sc = new OriginScene { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.spriteBatch, Font = this.Font,Texture=Loader.LoadTexture(this.GraphicsDevice,Config.BGPFile),Color=Color.White };
            sc.Rectangle=new Rectangle(0,0,this.graphics.PreferredBackBufferWidth,this.graphics.PreferredBackBufferHeight);

            sc.LoadContent();
            c = sc;
            
        }

        protected override void UnloadContent()
        {

        }
        protected override void Update(GameTime gameTime)
        {
            c.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.A))
                throw new Exception("string");

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            c.Draw(gameTime);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
