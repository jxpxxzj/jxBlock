﻿using Block.Audio;
using Block.Base;
using Block.Control;
using Block.Helper;
using Block.Game;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Block
{
    partial class OriginScene : Scene
    {
        Text timer;
        Text MouseStatus;
        Text MousePosition;
        Text OriMousePosition;
        Text ScoreInfo;
        Text Version;

        Checkbox NoFail;

        FpsCounter counter;

        KeyOverlay Keys1;
        KeyOverlay Keys2;
        MouseOverlay Mouse1;
        MouseOverlay Mouse2;

        PipeBase block;

        public FTFont Font{get ;set;}
        public OriginScene()
        {

        }
        public OriginScene(GraphicsDevice graphicsDevice,SpriteBatch spriteBatch)
        {
            this.GraphicsDevice = graphicsDevice;
            this.SpriteBatch = SpriteBatch;
            LoadContent();

        }
        public override void LoadContent()
        {
            base.LoadContent();

            timer = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Font = this.Font, Color = Config.FontColor, Position = new Vector2(60*Config.TileCount+10, 0) };
            timer.LoadContent();
            

            Version = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Font = this.Font, Color = Config.FontColor, Position = new Vector2(60 * Config.TileCount + 10, 580) };
            Version.LoadContent();
            Version.text = "Version 0.2    Made by jxpxxzj";

            MouseStatus = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.FontColor, Position = new Vector2(60 * Config.TileCount + 10, 40), Font = Font };
            MouseStatus.LoadContent();

            MousePosition = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.FontColor, Position = new Vector2(60 * Config.TileCount + 10, 20), Font = Font };
            MousePosition.LoadContent();

            OriMousePosition = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.FontColor, Position = new Vector2(60 * Config.TileCount + 10, 80), Font = Font };
            OriMousePosition.LoadContent();

            ScoreInfo = new Text { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.FontColor, Position = new Vector2(60 * Config.TileCount + 10, 60), Font = Font };
            ScoreInfo.LoadContent();

            Keys1 = new KeyOverlay { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(770, 240, 30, 30), Font = this.Font, KeyBinding = Keys.Z ,Text="K1"};
            Keys1.LoadContent();

            Keys2 = new KeyOverlay { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(770, 270, 30, 30), Font = this.Font, KeyBinding = Keys.X ,Text="K2"};
            Keys2.LoadContent();

            Mouse1 = new MouseOverlay { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(770, 300, 30, 30), Font = this.Font, MouseBinding = MouseOverlay.MouseButton.LeftButton, Text = "M1" };
            Mouse1.LoadContent();

            Mouse2 = new MouseOverlay { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(770, 330, 30, 30), Font = this.Font, MouseBinding = MouseOverlay.MouseButton.RightButton, Text = "M2" };
            Mouse2.LoadContent();

            NoFail = new Checkbox { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(60 * Config.TileCount + 10, 100, 20, 20), CheckStatus = Config.NoFail, Color = Config.FontColor, Font = this.Font, Text = "NoFail" };
            NoFail.LoadContent();

            counter = new FpsCounter { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Color = Config.FontColor, Font = this.Font, text = "", Position = new Vector2(760, 580) };

            Mod tempmod = new Mod {GraphicsDevice=this.GraphicsDevice,SpriteBatch=this.SpriteBatch, Auto = Config.Auto, Relax = Config.Relax, Hidden = Config.Hidden, HardRock = Config.HardRock, NoFail = Config.NoFail, FlashLight = Config.FlashLight,Rectangle=new Rectangle(600,100,0,0) };
            switch (Config.GameMode)
            {
                case 0:
                    block = new Classic { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(0, 0, 60, 80), LineLength = 0, MoveVelocity = Config.MoveVelocity, BlockCount = Config.TileCount, Mods = tempmod };
                   break;
                case 1:
                   block = new Arcade { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(0, 0, 60, 80), LineLength = 0, MoveVelocity = Config.MoveVelocity, BlockCount = Config.TileCount, Mods = tempmod };
                   break;
                case 2:
                   block = new Zen { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(0, 0, 60, 80), LineLength = 0, MoveVelocity = Config.MoveVelocity, BlockCount = Config.TileCount, Mods = tempmod };
                   break;
                default:
                   block = new Classic { GraphicsDevice = this.GraphicsDevice, SpriteBatch = this.SpriteBatch, Rectangle = new Rectangle(0, 0, 60, 80), LineLength = 0, MoveVelocity = Config.MoveVelocity, BlockCount = Config.TileCount, Mods = tempmod };
                   break;
            }
            block.LoadContent();
            block.Failed+=block_Failed;
            timer.text = string.Format("Mode:{0}  Move Velocity:{1}  Line Length:{2}",block.ToString(), Config.MoveVelocity, block.LineLength);
            player.Play(true);
        }

        public override void UnloadContent()
        {
            
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Keys1.Draw(gameTime);
            Keys2.Draw(gameTime);
            Mouse1.Draw(gameTime);
            Mouse2.Draw(gameTime);
            block.Draw(gameTime);
            block.Mods.Draw(gameTime);

            timer.Draw(gameTime);
            Version.Draw(gameTime);

            MouseStatus.text = string.Format("Score:{0} Accuracy:{1}% Speed:{2}/s Time:{3}ms", block.Score, Math.Round(block.Accuracy,2),Math.Round(block.  Speed,2),Math.Round(block.ElapsedMilliseconds,2));
            MouseStatus.Draw(gameTime);

            MousePosition.text = string.Format("Mouse Location:({0},{1}) Buffer:{2} Next:{3} Status:{4}", MouseLocationX, MouseLocationY,block.BlockLine.Count,block.NextIndex,block.Status.ToString());
            MousePosition.Draw(gameTime);

            OriMousePosition.text = string.Format("Black:{0} White:{1} Miss:{2}", block.BlackCount, block.WhiteCount, block.MissCount);
            OriMousePosition.Draw(gameTime);

            ScoreInfo.text = string.Format("Total Combo:{0}  Max Combo:{1}", block.TotalCombo, block.MaxCombo);
            ScoreInfo.Draw(gameTime);

            counter.Draw(gameTime);
            NoFail.Draw(gameTime);

        }
        public override void Update(GameTime gameTime)
        {
            MouseLocationX = Mouse.GetState().X;
            MouseLocationY = Mouse.GetState().Y;
            counter.Update(gameTime);
            Keys1.Update(gameTime);
            Keys2.Update(gameTime);
            Mouse1.Update(gameTime);
            Mouse2.Update(gameTime);
            NoFail.Update(gameTime);
            block.Mods.NoFail = NoFail.CheckStatus;
            block.Update(gameTime);

            base.Update(gameTime);
            if (Keyboard.GetState().IsKeyDown(Keys.S))
                block.Start();
            if (Keyboard.GetState().IsKeyDown(Keys.P))
                block.Stop();
        }
    }
    partial class OriginScene : Scene
    {
        int MouseLocationX = Mouse.GetState().X;
        int MouseLocationY = Mouse.GetState().Y;

        AudioPlayer player = new AudioPlayer { Stream = Loader.LoadAudioStream(Config.BGMFile,true) };

        List<DisappearImage> TrailImage=new List<DisappearImage>();
        void RefreshPosition()
        {
            MouseLocationX = Mouse.GetState().X;
            MouseLocationY = Mouse.GetState().Y;
        }

        void block_Failed(object sender, EventArgs e)
        {
            block.Stop();
        }
    }
}
