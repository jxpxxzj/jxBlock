﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Block.Helper;

namespace Block.Base
{
    class Text : Component
    {
        public string text { get; set; } 
        public Color Color { get; set; }
        public Vector2 Position { get; set; }
        public FTFont Font { get; set; }
        public override void Draw(GameTime gameTime)
        {
            Font.DrawText(SpriteBatch, Position, text, Color);
            //SpriteBatch.DrawString(Font, text, Position, Color);
        }
        public override void LoadContent()
        {
           
        }
        public override void UnloadContent()
        {
            
        }
        public override void Update(GameTime gameTime)
        {
           
        }
    }
}
