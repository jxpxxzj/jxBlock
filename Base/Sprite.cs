﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;



namespace Block.Base
{
    class Sprite :Component
    {
        public Texture2D Texture { get; set; }
        public Rectangle Rectangle { get; set; }

        public Color Color { get; set; }

        public override void Draw(GameTime gameTime)
        {
            //if (Texture!=null && Rectangle!=null && Color!=null)
                SpriteBatch.Draw(Texture, Rectangle, Color);
        }
        public override void LoadContent()
        {
           
        }
        public override void UnloadContent()
        {
           
        }
        public override void Update(GameTime gameTime)
        {

        }


    }
}
