﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Un4seen.Bass;

namespace Block.Audio
{
    class AudioPlayer :IDisposable
    {
        public AudioPlayer()
        {
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT,IntPtr.Zero);
        }
        public AudioStream Stream { get; set; }

        public void Play(bool restart)
        {
            Bass.BASS_ChannelPlay(Stream.StreamNumber, restart);
        }
        public void Pause()
        {
            Bass.BASS_ChannelPause(Stream.StreamNumber);
        }
        public void Stop()
        {
            Bass.BASS_ChannelStop(Stream.StreamNumber);
        }
        void IDisposable.Dispose()
        {
            Bass.BASS_StreamFree(Stream.StreamNumber);
            Bass.BASS_Stop();
            Bass.BASS_Free();
        }

    }
    class AudioStream
    {
        public int StreamNumber { get; set; }
        public string StreamName { get; protected set; }
        public AudioStream(string FileName,bool Loop=false)
        {
            if(Loop==false)
                StreamNumber = Bass.BASS_StreamCreateFile(FileName, 0L, 0L, BASSFlag.BASS_SAMPLE_FLOAT);
            else
                StreamNumber = Bass.BASS_StreamCreateFile(FileName, 0L, 0L, BASSFlag.BASS_SAMPLE_LOOP);
            StreamName = FileName;
        }
    }

}
